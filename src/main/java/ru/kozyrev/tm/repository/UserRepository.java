package ru.kozyrev.tm.repository;

import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.entity.EntityException;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class UserRepository extends AbstractRepository<User> {
    private Map<String, User> map = new LinkedHashMap<>();

    @Override
    public List<User> findAll() {
        return new ArrayList<>(map.values());
    }

    @Override
    public User findOne(String id) {
        return map.get(id);
    }

    @Override
    public User persist(User user) throws Exception {
        if (map.containsKey(user.getId())) {
            throw new EntityException();
        }
        map.put(user.getId(), user);
        return user;
    }

    @Override
    public User merge(User user) {
        map.put(user.getId(), user);
        return user;
    }

    @Override
    public User remove(String id) {
        return map.remove(id);
    }

    @Override
    public void removeAll() {
        map.clear();
    }
}
