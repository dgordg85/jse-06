package ru.kozyrev.tm.repository;

import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.exception.entity.EntityException;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ProjectRepository extends AbstractRepository<Project> {
    private Map<String, Project> map = new LinkedHashMap<>();

    @Override
    protected List<Project> findAll() {
        return new ArrayList<>(map.values());
    }

    @Override
    protected Project findOne(String id) {
        return map.get(id);
    }

    @Override
    protected Project persist(Project project) throws EntityException {
        if (map.containsKey(project.getId())) {
            throw new EntityException();
        }
        map.put(project.getId(), project);
        return project;
    }

    @Override
    protected Project merge(Project project) {
        map.put(project.getId(), project);
        return project;
    }

    @Override
    protected Project remove(String id) {
        return map.remove(id);
    }

    @Override
    protected void removeAll() {
        map.clear();
    }

    public Project findOne(String id, String userId) {
        Project project = findOne(id);
        return project.getUserId().equals(userId) ? project : null;
    }

    public List<Project> findAll(String userId) {
        List<Project> list = new ArrayList<>();
        List<Project> allProjects = findAll();
        for (Project project : allProjects) {
            if (project.getUserId().equals(userId)) {
                list.add(project);
            }
        }
        return list;
    }

    public Project merge(Project project, String userId) {
        Project projectUpdate = findOne(project.getId());
        if (projectUpdate.getUserId().equals(userId)) {
            merge(project);
            return project;
        }
        return null;
    }

    public Project remove(String id, String userId) {
        Project project = findOne(id);
        if (project.getUserId().equals(userId)) {
            return remove(id);
        }
        return null;
    }

    public void removeAll(String userId) {
        List<Project> allProjects = findAll();
        for (Project project : allProjects) {
            if (project.getUserId().equals(userId)) {
                remove(project.getId());
            }
        }
    }

    public Project persist(Project project, String userId) throws EntityException {
        persist(project);
        return project;
    }
}
