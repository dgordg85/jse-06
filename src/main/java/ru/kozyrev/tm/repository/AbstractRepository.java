package ru.kozyrev.tm.repository;

import java.util.List;

public abstract class AbstractRepository<T> {
    protected abstract List<T> findAll();

    protected abstract T findOne(String id);

    protected abstract T persist(T abstractEntity) throws Exception;

    protected abstract T merge(T abstractEntity);

    protected abstract T remove(String id);

    protected abstract void removeAll();
}
