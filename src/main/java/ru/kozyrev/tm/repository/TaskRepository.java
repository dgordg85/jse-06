package ru.kozyrev.tm.repository;

import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.exception.entity.EntityException;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository extends AbstractRepository<Task> {
    private Map<String, Task> map = new LinkedHashMap<>();

    @Override
    protected List<Task> findAll() {
        return new ArrayList<>(map.values());
    }

    @Override
    protected Task findOne(String id) {
        return map.get(id);
    }

    @Override
    protected Task persist(Task task) throws Exception {
        if (map.containsKey(task.getId())) {
            throw new EntityException();
        }
        map.put(task.getId(), task);
        return task;
    }

    @Override
    protected Task merge(Task task) {
        map.put(task.getId(), task);
        return task;
    }

    @Override
    protected Task remove(String id) {
        return map.remove(id);
    }

    @Override
    protected void removeAll() {
        map.clear();
    }

    public Task findOne(String id, String userId) {
        Task task = findOne(id);
        return task.getUserId().equals(userId) ? task : null;
    }

    public List<Task> findAll(String userId) {
        List<Task> list = new ArrayList<>();
        List<Task> allTasks = findAll();
        for (Task task : allTasks) {
            if (task.getUserId().equals(userId)) {
                list.add(task);
            }
        }
        return list;
    }

    public Task merge(Task task, String userId) {
        Task taskUpdate = findOne(task.getId());
        if (taskUpdate.getUserId().equals(userId)) {
            merge(task);
            return task;
        }
        return null;
    }

    public Task remove(String id, String userId) {
        Task task = findOne(id);
        if (task.getUserId().equals(userId)) {
            return remove(id);
        }
        return null;
    }

    public void removeAll(String userId) {
        List<Task> allTasks = findAll();
        for (Task task : allTasks) {
            if (task.getUserId().equals(userId)) {
                remove(task.getId());
            }
        }
    }

    public Task persist(Task task, String userId) throws Exception {
        persist(task);
        return task;
    }
}
