package ru.kozyrev.tm.service;

import ru.kozyrev.tm.context.Bootstrap;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.System.NoUserAuthException;
import ru.kozyrev.tm.exception.entity.*;
import ru.kozyrev.tm.repository.TaskRepository;
import ru.kozyrev.tm.util.StringUtil;

import java.util.Iterator;
import java.util.List;

public class TaskService extends AbstractService<Task> {
    TaskRepository taskRepository;
    ProjectService projectService;
    Bootstrap bootstrap;

    public TaskService(Bootstrap bootstrap) {
        this.taskRepository = bootstrap.getTaskRepository();
        this.projectService = bootstrap.getProjectService();
        this.bootstrap = bootstrap;
    }

    @Override
    public Task findOne(String id) throws Exception {
        if (id == null || id.isEmpty())
            return null;
        return taskRepository.findOne(id, getCurrentUser());
    }

    @Override
    public List<Task> findAll() throws Exception {
        return taskRepository.findAll(getCurrentUser());
    }

    @Override
    public Task persist(Task task) throws Exception {
        if (task.getName() == null || task.getName().isEmpty()) {
            throw new NameException();
        }
        if (task.getUserId() == null || task.getUserId().isEmpty()) {
            throw new EntityOwnerException();
        }
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
            throw new IndexException();
        }
        if (task.getDescription() == null || task.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (task.getDateStart() == null) {
            throw new DateException();
        }
        if (task.getDateFinish() == null) {
            throw new DateException();
        }
        return taskRepository.persist(task, getCurrentUser());
    }

    @Override
    public Task merge(Task task) throws Exception {
        if (task.getName() == null) {
            throw new NameException();
        }
        if (task.getUserId() == null || task.getUserId().isEmpty()) {
            throw new EntityOwnerException();
        }
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
            throw new IndexException();
        }
        if (task.getDescription() == null) {
            throw new DescriptionException();
        }
        if (task.getDateStart() == null) {
            throw new DateException();
        }
        if (task.getDateFinish() == null) {
            throw new DateException();
        }

        Task taskUpdate = findOne(task.getId());

        if (!task.getName().isEmpty()) {
            taskUpdate.setName(task.getName());
        }
        taskUpdate.setProjectId(task.getProjectId());

        if (!task.getDescription().isEmpty())
            taskUpdate.setDescription(task.getDescription());

        taskUpdate.setDateStart(task.getDateStart());
        taskUpdate.setDateFinish(task.getDateFinish());
        return taskRepository.merge(taskUpdate, getCurrentUser());
    }

    @Override
    public Task remove(String taskNum) throws Exception {
        if (taskNum == null || taskNum.isEmpty()) {
            return null;
        }
        return taskRepository.remove(getTaskIdByNum(taskNum), getCurrentUser());
    }

    public boolean removeProjectTasks(String projectNum) throws Exception {
        boolean isDelete = false;
        String projectId = projectService.getProjectIdByNum(projectNum);
        List<Task> list = findAll();
        Iterator<Task> iterator = list.iterator();
        Task task;
        while (iterator.hasNext()) {
            task = iterator.next();
            if (task.getProjectId().equals(projectId)) {
                taskRepository.remove(task.getId(), getCurrentUser());
                isDelete = true;
            }
        }
        return isDelete;
    }

    @Override
    public void removeAll() throws Exception {
        taskRepository.removeAll(getCurrentUser());
    }

    public Task getTaskByNum(String num) throws Exception {
        if (num == null || num.isEmpty()) {
            throw new IndexException();
        }

        int index = StringUtil.parseToInt(num) - 1;
        List<Task> list = findAll();

        if (index < 0 || index > list.size() - 1) {
            throw new IndexException();
        }
        return list.get(index);
    }

    public String getTaskIdByNum(String num) throws Exception {
        return getTaskByNum(num).getId();
    }

    private String getCurrentUser() throws Exception {
        User user = bootstrap.getCurrentUser();
        if (user == null) {
            throw new NoUserAuthException();
        }
        return user.getId();
    }
}
