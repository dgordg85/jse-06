package ru.kozyrev.tm.service;

import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.user.UserLoginEmptyException;
import ru.kozyrev.tm.exception.user.UserLoginTakenException;
import ru.kozyrev.tm.exception.user.UserNoRoleException;
import ru.kozyrev.tm.exception.user.UserPasswordEmptyException;
import ru.kozyrev.tm.repository.UserRepository;
import ru.kozyrev.tm.util.HashUtil;

import java.util.List;

public class UserService extends AbstractService<User> {
    UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findOne(String id) {
        if (id == null || id.isEmpty()) {
            return null;
        }
        return userRepository.findOne(id);
    }

    @Override
    public User persist(User user) throws Exception {
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            throw new UserLoginEmptyException();
        }
        if (getUserByLogin(user.getLogin()) != null) {
            throw new UserLoginTakenException();
        }
        if (user.getPasswordHash() == null || user.getPasswordHash().isEmpty() || user.getPasswordHash().equals(HashUtil.EMPTY_PASSWORD)) {
            throw new UserPasswordEmptyException();
        }
        return userRepository.persist(user);
    }

    @Override
    public User merge(User user) throws Exception {
        if (user.getLogin() == null) {
            throw new UserLoginEmptyException();
        }
        if (user.getPasswordHash() == null) {
            throw new UserPasswordEmptyException();
        }
        if (user.getRoleType() == null) {
            throw new UserNoRoleException();
        }
        User userUpdate = findOne(user.getId());

        if (!user.getLogin().isEmpty()) {
            userUpdate.setLogin(user.getLogin());
        }

        if (user.getPasswordHash().isEmpty() || user.getPasswordHash().equals(HashUtil.EMPTY_PASSWORD)) {
            throw new UserPasswordEmptyException();
        }
        userUpdate.setPasswordHash(user.getPasswordHash());
        userUpdate.setRoleType(user.getRoleType());

        return userRepository.merge(userUpdate);
    }

    @Override
    public User remove(String id) {
        if (id == null || id.isEmpty()) {
            return null;
        }
        return userRepository.remove(id);
    }

    @Override
    public void removeAll() {
        userRepository.removeAll();
    }

    public User getUserByLogin(String login) {
        if (login == null || login.isEmpty()) {
            return null;
        }
        List<User> list = findAll();
        for (User user : list) {
            if (user.getLogin().equals(login)) {
                return user;
            }
        }
        return null;
    }

    private String getUserIdByLogin(String login) {
        return getUserByLogin(login).getId();
    }

    public boolean isPasswordNotTrue(String userId, String password) {
        return !findOne(userId).getPasswordHash().equals(HashUtil.getHash(password));
    }
}
