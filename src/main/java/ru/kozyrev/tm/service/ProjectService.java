package ru.kozyrev.tm.service;

import ru.kozyrev.tm.context.Bootstrap;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.System.NoUserAuthException;
import ru.kozyrev.tm.exception.entity.*;
import ru.kozyrev.tm.repository.ProjectRepository;
import ru.kozyrev.tm.util.StringUtil;

import java.util.List;

public class ProjectService extends AbstractService<Project> {
    ProjectRepository projectRepository;
    Bootstrap bootstrap;

    public ProjectService(Bootstrap bootstrap) {
        this.projectRepository = bootstrap.getProjectRepository();
        this.bootstrap = bootstrap;
    }

    @Override
    public Project findOne(String id) throws Exception {
        if (id == null || id.length() == 0)
            return null;
        return projectRepository.findOne(id, getCurrentUserId());
    }

    @Override
    public List<Project> findAll() throws Exception {
        return projectRepository.findAll(getCurrentUserId());
    }

    @Override
    public Project persist(Project project) throws Exception {
        if (project.getName() == null || project.getName().isEmpty()) {
            throw new NameException();
        }
        if (project.getUserId() == null || project.getUserId().isEmpty()) {
            throw new EntityOwnerException();
        }
        if (project.getDescription() == null || project.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (project.getDateStart() == null) {
            throw new DateException();
        }
        if (project.getDateFinish() == null) {
            throw new DateException();
        }
        return projectRepository.persist(project, project.getUserId());
    }

    @Override
    public Project merge(Project project) throws Exception {
        if (project.getName() == null) {
            throw new NameException();
        }
        if (project.getUserId() == null || project.getUserId().isEmpty()) {
            throw new EntityOwnerException();
        }
        if (project.getDescription() == null) {
            throw new DescriptionException();
        }
        if (project.getDateStart() == null) {
            throw new DateException();
        }
        if (project.getDateFinish() == null) {
            throw new DateException();
        }
        Project projectUpdate = findOne(project.getId());
        if (!project.getName().isEmpty()) {
            projectUpdate.setName(project.getName());
        }
        if (!project.getDescription().isEmpty()) {
            projectUpdate.setDescription(project.getDescription());
        }
        projectUpdate.setDateStart(project.getDateStart());
        projectUpdate.setDateFinish(project.getDateFinish());
        return projectRepository.merge(projectUpdate, getCurrentUserId());
    }

    @Override
    public Project remove(String num) throws Exception {
        if (num == null || num.isEmpty())
            return null;
        return projectRepository.remove(getProjectIdByNum(num), getCurrentUserId());
    }

    @Override
    public void removeAll() throws Exception {
        projectRepository.removeAll(getCurrentUserId());
    }

    public Project getProjectByNum(String num) throws Exception {
        if (num == null || num.isEmpty()) {
            throw new IndexException();
        }

        int index = StringUtil.parseToInt(num) - 1;
        List<Project> list = findAll();

        if (index < 0 || index > list.size() - 1) {
            throw new IndexException();
        }
        return list.get(index);
    }

    public String getProjectIdByNum(String num) throws Exception {
        return getProjectByNum(num).getId();
    }

    private String getCurrentUserId() throws Exception {
        User user = bootstrap.getCurrentUser();
        if (user == null) {
            throw new NoUserAuthException();
        }
        return user.getId();
    }
}
