package ru.kozyrev.tm.service;

import java.util.List;

public abstract class AbstractService<T> {
    public abstract List<T> findAll() throws Exception;

    public abstract T findOne(String id) throws Exception;

    public abstract T persist(T abstractEntity) throws Exception;

    public abstract T merge(T abstractEntity) throws Exception;

    public abstract T remove(String id) throws Exception;

    public abstract void removeAll() throws Exception;
}
