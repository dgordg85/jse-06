package ru.kozyrev.tm.exception.System;

public class NoUserAuthException extends Exception {
    public NoUserAuthException() {
        super("User is not auth!");
    }
}
