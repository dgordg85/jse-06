package ru.kozyrev.tm.exception.user;

public class UserPasswordChangeException extends Exception {
    public UserPasswordChangeException() {
        super("Password not update");
    }
}
