package ru.kozyrev.tm.exception.user;

public class UserLoginTakenException extends Exception {
    public UserLoginTakenException() {
        super("Login is taken! User another login!");
    }
}
