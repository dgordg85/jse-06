package ru.kozyrev.tm.exception.user;

public class UserPasswordWrongException extends Exception {
    public UserPasswordWrongException() {
        super("Wrong password!");
    }
}
