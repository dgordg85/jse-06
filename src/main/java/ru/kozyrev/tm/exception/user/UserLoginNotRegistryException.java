package ru.kozyrev.tm.exception.user;

public class UserLoginNotRegistryException extends Exception {
    public UserLoginNotRegistryException() {
        super("User not registry!");
    }
}
