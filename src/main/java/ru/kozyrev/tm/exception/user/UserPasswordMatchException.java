package ru.kozyrev.tm.exception.user;

public class UserPasswordMatchException extends Exception {
    public UserPasswordMatchException() {
        super("Password don't match!");
    }
}
