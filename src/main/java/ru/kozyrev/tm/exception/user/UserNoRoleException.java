package ru.kozyrev.tm.exception.user;

public class UserNoRoleException extends Exception {
    public UserNoRoleException() {
        super("No role for user!");
    }
}
