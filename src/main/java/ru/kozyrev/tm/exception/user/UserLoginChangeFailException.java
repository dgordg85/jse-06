package ru.kozyrev.tm.exception.user;

public class UserLoginChangeFailException extends Exception {
    public UserLoginChangeFailException() {
        super("Can't change login. Try again");
    }
}
