package ru.kozyrev.tm.exception.user;

public class UserLoginEmptyException extends Exception {
    public UserLoginEmptyException() {
        super("Login is empty!");
    }
}
