package ru.kozyrev.tm.exception.user;

public class UserPasswordEmptyException extends Exception {
    public UserPasswordEmptyException() {
        super("Password is empty!");
    }
}
