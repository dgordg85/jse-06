package ru.kozyrev.tm.exception.command;

public class AccessForbiddenException extends Exception {
    public AccessForbiddenException() {
        super("Access forbidden for this command!");
    }
}
