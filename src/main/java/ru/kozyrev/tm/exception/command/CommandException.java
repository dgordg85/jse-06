package ru.kozyrev.tm.exception.command;

public class CommandException extends Exception {
    public CommandException() {
        super("Wrong command! User 'help' command!");
    }
}
