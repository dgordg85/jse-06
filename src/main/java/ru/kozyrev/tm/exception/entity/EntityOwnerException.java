package ru.kozyrev.tm.exception.entity;

public class EntityOwnerException extends Exception {
    public EntityOwnerException() {
        super("Owner is empty!");
    }
}
