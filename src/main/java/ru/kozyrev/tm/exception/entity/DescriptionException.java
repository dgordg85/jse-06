package ru.kozyrev.tm.exception.entity;

public class DescriptionException extends Exception {
    public DescriptionException() {
        super("Wrong Description");
    }
}
