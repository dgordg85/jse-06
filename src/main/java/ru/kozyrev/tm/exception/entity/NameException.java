package ru.kozyrev.tm.exception.entity;

public class NameException extends Exception {
    public NameException() {
        super("Wrong name!");
    }
}
