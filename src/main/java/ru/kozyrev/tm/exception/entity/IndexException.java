package ru.kozyrev.tm.exception.entity;

public class IndexException extends Exception {
    public IndexException() {
        super("Wrong index!");
    }
}
