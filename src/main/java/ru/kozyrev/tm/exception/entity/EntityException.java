package ru.kozyrev.tm.exception.entity;

public class EntityException extends Exception {
    public EntityException() {
        super("Entity work error!");
    }
}
