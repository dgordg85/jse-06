package ru.kozyrev.tm.exception.entity;

public class DateException extends Exception {
    public DateException() {
        super("Wrong date! Use dd-MM-YYYY format!");
    }
}
