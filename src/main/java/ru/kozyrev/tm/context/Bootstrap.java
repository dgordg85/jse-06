package ru.kozyrev.tm.context;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.command.project.*;
import ru.kozyrev.tm.command.system.ExitCommand;
import ru.kozyrev.tm.command.system.HelpCommand;
import ru.kozyrev.tm.command.task.*;
import ru.kozyrev.tm.command.user.*;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.command.AccessForbiddenException;
import ru.kozyrev.tm.exception.command.CommandCorruptException;
import ru.kozyrev.tm.exception.command.CommandException;
import ru.kozyrev.tm.exception.entity.DateException;
import ru.kozyrev.tm.exception.entity.IndexException;
import ru.kozyrev.tm.repository.ProjectRepository;
import ru.kozyrev.tm.repository.TaskRepository;
import ru.kozyrev.tm.repository.UserRepository;
import ru.kozyrev.tm.service.ProjectService;
import ru.kozyrev.tm.service.TaskService;
import ru.kozyrev.tm.service.UserService;
import ru.kozyrev.tm.util.HashUtil;

import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Bootstrap {
    private ProjectRepository projectRepository;
    private ProjectService projectService;
    private UserService userService;
    private UserRepository userRepository;
    private TaskRepository taskRepository;
    private TaskService taskService;
    private Scanner sc = new Scanner(System.in);
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private User currentUser = null;

    public Bootstrap() {
        projectRepository = new ProjectRepository();
        taskRepository = new TaskRepository();
        projectService = new ProjectService(this);
        taskService = new TaskService(this);
        userRepository = new UserRepository();
        userService = new UserService(userRepository);
    }

    public void init() throws Exception {
        initCommands();
        createDefaultUsers();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command;
        while (true) {
            try {
                command = sc.nextLine().toLowerCase().replace('_', '-');
                execute(command);
            } catch (ParseException | DateException e) {
                System.out.println("Wrong date! User dd-MM-YYYY format");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public void execute(String commandStr) throws Exception {
        if (commandStr == null || commandStr.isEmpty()) {
            throw new CommandException();
        }
        AbstractCommand command = commands.get(commandStr);
        if (isCommandNotAllow(command)) {
            throw new AccessForbiddenException();
        }
        command.execute();
    }

    public void initCommands() throws CommandCorruptException {
        registryCommand(new UserCreateCommand());
        registryCommand(new UserLoginCommand());
        registryCommand(new UserLogoutCommand());
        registryCommand(new UserProfileCommand());
        registryCommand(new UserUpdateCommand());
        registryCommand(new UserRemoveCommand());
        registryCommand(new UserPasswordUpdateCommand());
        registryCommand(new ProjectListCommand());
        registryCommand(new ProjectCreateCommand());
        registryCommand(new ProjectUpdateCommand());
        registryCommand(new ProjectRemoveCommand());
        registryCommand(new ProjectClearCommand());
        registryCommand(new TaskListCommand());
        registryCommand(new TaskClearCommand());
        registryCommand(new TaskCreateCommand());
        registryCommand(new TaskUpdateCommand());
        registryCommand(new TaskRemoveCommand());
        registryCommand(new HelpCommand());
        registryCommand(new ExitCommand());
    }

    public void printTasks() throws Exception {
        System.out.println("[TASKS LIST]");
        List<Task> taskList = getTaskService().findAll();
        for (int i = 0; i < taskList.size(); i++) {
            Task task = taskList.get(i);
            System.out.printf("%d. %s, %s, s:%s, f:%s\n",
                    i + 1,
                    task.getName(),
                    task.getDescription(),
                    task.getDateStartAsStr(),
                    task.getDateFinishAsStr()
            );
        }
    }

    public void printTasks(String projectNum) throws Exception {
        String projectId = getProjectService().getProjectIdByNum(projectNum);
        if (projectId == null)
            throw new IndexException();
        System.out.println("[TASKS LIST OF PROJECT]");
        List<Task> taskList = getTaskService().findAll();
        int count = 1;
        for (int i = 0; i < taskList.size(); i++) {
            if (taskList.get(i).getProjectId().equals(projectId)) {
                Task task = taskList.get(i);
                System.out.printf("%d. %s, EDIT ID#%d, %s, s:%s, f:%s\n",
                        count++,
                        task.getName(),
                        i,
                        task.getDescription(),
                        task.getDateStartAsStr(),
                        task.getDateFinishAsStr()
                );
            }
        }
    }

    public void printProjects() throws Exception {
        System.out.println("[PROJECTS LIST]");
        List<Project> projects = getProjectService().findAll();
        for (int i = 0; i < projects.size(); i++) {
            Project project = projects.get(i);
            System.out.printf("%d. %s, %s, s:%s, f:%s\n",
                    i + 1,
                    project.getName(),
                    project.getDescription(),
                    project.getDateStartAsStr(),
                    project.getDateFinishAsStr()
            );
        }
    }

    public void clearProject(String projectNum) throws Exception {
        clearTasks(projectNum);
        if (projectService.remove(projectNum) == null) {
            throw new IndexException();
        }
        System.out.println("[PROJECT REMOVED]");
        printProjects();
    }

    public void clearTasks(String projectNum) throws Exception {
        if (getTaskService().removeProjectTasks(projectNum)) {
            System.out.println("[ALL TASKS OF PROJECT REMOVED]");
        } else {
            System.out.println("[PROJECT HAVE NO TASKS]");
        }
    }

    public void clearAllTasks() throws Exception {
        getTaskService().removeAll();
        System.out.println("[ALL TASKS REMOVED]");
    }

    private void registryCommand(AbstractCommand command) throws CommandCorruptException {
        if (command.getName() == null || command.getName().isEmpty())
            throw new CommandCorruptException();
        if (command.getDescription() == null || command.getDescription().isEmpty())
            throw new CommandCorruptException();
        command.init(this);
        commands.put(command.getName(), command);
    }

    private boolean isCommandNotAllow(AbstractCommand command) throws CommandException {
        if (command == null) {
            throw new CommandException();
        }
        boolean isSecureCheck = command.isSecure() || isUserAuth();

        boolean isRoleAllowed;
        if (getCurrentUser() != null) {
            isRoleAllowed = command.getRoleTypes().contains(getCurrentUser().getRoleType());
        } else
            isRoleAllowed = true;

        if (!isSecureCheck || !isRoleAllowed) {
            return true;
        }
        return false;
    }

    public void printCommands() {
        for (AbstractCommand command : commands.values()) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }

    public void createDefaultUsers() throws Exception {
        User user = new User();
        user.setLogin("user1");
        user.setPasswordHash(HashUtil.getHash("user1"));
        user.setRoleType(RoleType.USER);
        userService.persist(user);

        User admin = new User();
        admin.setLogin("admin");
        admin.setPasswordHash(HashUtil.getHash("admin"));
        admin.setRoleType(RoleType.ADMIN);
        userService.persist(admin);
    }

    public Scanner getSc() {
        return sc;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public UserService getUserService() {
        return userService;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    public void setSc(Scanner sc) {
        this.sc = sc;
    }

    private boolean isUserAuth() {
        return currentUser != null;
    }
}
