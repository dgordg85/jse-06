package ru.kozyrev.tm.command.user;

import ru.kozyrev.tm.command.AbstractCommand;

public class UserRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public String getDescription() {
        return "Use for deleting profile";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER REMOVE]");
        System.out.println("Are you sure? Type 'yes' or another for cancel...");
        if (bootstrap.getSc().nextLine().toLowerCase().equals("yes")) {
            String userId = bootstrap.getCurrentUser().getId();
            bootstrap.getUserService().remove(userId);
            System.out.println("[USER DELETE!]");
            bootstrap.setCurrentUser(null);
        } else {
            System.out.println("[OPERATION ABORT!]");
        }
    }
}
