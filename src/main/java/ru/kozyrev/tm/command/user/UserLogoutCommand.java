package ru.kozyrev.tm.command.user;

import ru.kozyrev.tm.command.AbstractCommand;

public class UserLogoutCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "Use for logout.";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.setCurrentUser(null);
        System.out.println("[User logout!]");
    }
}
