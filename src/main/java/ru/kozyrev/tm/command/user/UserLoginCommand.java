package ru.kozyrev.tm.command.user;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.user.UserLoginNotRegistryException;
import ru.kozyrev.tm.exception.user.UserPasswordWrongException;

public class UserLoginCommand extends AbstractCommand {
    public UserLoginCommand() {
        secure = true;
    }

    @Override
    public String getName() {
        return "user-login";
    }

    @Override
    public String getDescription() {
        return "Use for logging in system.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGIN]");

        System.out.println("ENTER LOGIN:");
        String login = bootstrap.getSc().nextLine();

        User user = bootstrap.getUserService().getUserByLogin(login);
        if (user == null) {
            throw new UserLoginNotRegistryException();
        }
        System.out.println("ENTER PASSWORD:");
        String password = bootstrap.getSc().nextLine();

        if (bootstrap.getUserService().isPasswordNotTrue(user.getId(), password)) {
            throw new UserPasswordWrongException();
        }
        bootstrap.setCurrentUser(user);
        System.out.println("[User just login!]");
    }
}
