package ru.kozyrev.tm.command.user;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.entity.EntityException;
import ru.kozyrev.tm.exception.user.UserPasswordMatchException;
import ru.kozyrev.tm.util.HashUtil;

public class UserCreateCommand extends AbstractCommand {
    public UserCreateCommand() {
        secure = true;
    }

    @Override
    public String getName() {
        return "user-create";
    }

    @Override
    public String getDescription() {
        return "User for registry new user";
    }

    @Override
    public void execute() throws Exception {
        User user = new User();

        System.out.println("[USER REGISTRATION]");

        System.out.println("ENTER LOGIN:");
        user.setLogin(bootstrap.getSc().nextLine());

        System.out.println("ENTER PASSWORD:");
        String password = bootstrap.getSc().nextLine();

        System.out.println("RE-ENTER PASSWORD:");
        String password2 = bootstrap.getSc().nextLine();

        if (!password.equals(password2)) {
            throw new UserPasswordMatchException();
        }
        user.setPasswordHash(HashUtil.getHash(password));
        if (bootstrap.getUserService().persist(user) == null) {
            throw new EntityException();
        }
        System.out.println("[OK]");
    }
}
