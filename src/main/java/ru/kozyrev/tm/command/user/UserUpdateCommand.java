package ru.kozyrev.tm.command.user;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.user.UserLoginChangeFailException;
import ru.kozyrev.tm.exception.user.UserLoginEmptyException;

public class UserUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-update";
    }

    @Override
    public String getDescription() {
        return "Update user login.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE USER]");
        System.out.println("ENTER NEW LOGIN:");
        System.out.println("ENTER CURRENT PASSWORD:");
        String login = bootstrap.getSc().nextLine();
        if (login.isEmpty()) {
            throw new UserLoginEmptyException();
        }
        User user = new User();
        user.setId(bootstrap.getCurrentUser().getId());
        user.setLogin(login);
        user.setPasswordHash(bootstrap.getCurrentUser().getPasswordHash());
        user.setRoleType(bootstrap.getCurrentUser().getRoleType());
        user = bootstrap.getUserService().merge(user);
        if (user == null) {
            throw new UserLoginChangeFailException();
        }
        bootstrap.setCurrentUser(user);
        System.out.println("[PASSWORD UPDATE]");
    }
}
