package ru.kozyrev.tm.command.user;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.user.UserPasswordChangeException;
import ru.kozyrev.tm.exception.user.UserPasswordMatchException;
import ru.kozyrev.tm.exception.user.UserPasswordWrongException;
import ru.kozyrev.tm.util.HashUtil;

public class UserPasswordUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-pass-update";
    }

    @Override
    public String getDescription() {
        return "Use for updating password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PASSWORD]");

        System.out.println("ENTER CURRENT PASSWORD:");
        String password = bootstrap.getSc().nextLine();

        String userId = bootstrap.getCurrentUser().getId();
        if (bootstrap.getUserService().isPasswordNotTrue(userId, password)) {
            throw new UserPasswordWrongException();
        }

        System.out.println("ENTER NEW PASSWORD:");
        String newPassword = bootstrap.getSc().nextLine();

        System.out.println("RE-ENTER PASSWORD:");
        String reNewPassword = bootstrap.getSc().nextLine();

        if (!newPassword.equals(reNewPassword)) {
            throw new UserPasswordMatchException();
        }
        User user = new User();
        user.setId(bootstrap.getCurrentUser().getId());
        user.setLogin(bootstrap.getCurrentUser().getLogin());
        user.setPasswordHash(HashUtil.getHash(reNewPassword));
        user.setRoleType(bootstrap.getCurrentUser().getRoleType());
        user = bootstrap.getUserService().merge(user);
        if (user == null) {
            throw new UserPasswordChangeException();
        }
        bootstrap.setCurrentUser(user);
        System.out.println("[PASSWORD UPDATE]");
    }
}
