package ru.kozyrev.tm.command.user;

import ru.kozyrev.tm.command.AbstractCommand;

public class UserProfileCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-profile";
    }

    @Override
    public String getDescription() {
        return "Show user profile";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER PROFILE]");
        System.out.printf("Login: %s\n", bootstrap.getCurrentUser().getLogin());
        System.out.printf("Role: %s\n", bootstrap.getCurrentUser().getRoleType().getDisplayName());
    }
}
