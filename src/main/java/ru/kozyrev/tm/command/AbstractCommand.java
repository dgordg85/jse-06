package ru.kozyrev.tm.command;


import ru.kozyrev.tm.context.Bootstrap;
import ru.kozyrev.tm.enumerated.RoleType;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCommand {
    protected Bootstrap bootstrap;
    protected Boolean secure;
    private List<RoleType> roleTypes = new ArrayList<>();

    public void init(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public AbstractCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        this.secure = false;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public Boolean isSecure() {
        return secure;
    }

    public List<RoleType> getRoleTypes() {
        return roleTypes;
    }
}
