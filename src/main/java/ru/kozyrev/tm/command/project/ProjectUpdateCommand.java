package ru.kozyrev.tm.command.project;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.exception.entity.EntityException;
import ru.kozyrev.tm.util.DateUtil;

public class ProjectUpdateCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-update";
    }

    @Override
    public String getDescription() {
        return "Update Selected project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROJECT]\nENTER ID:");
        Project project = new Project();
        project.setId(bootstrap.getProjectService().getProjectIdByNum(bootstrap.getSc().nextLine()));
        System.out.println("ENTER NAME:");
        project.setName(bootstrap.getSc().nextLine());
        System.out.println("ENTER DESCRIPTION:");
        project.setDescription(bootstrap.getSc().nextLine());
        System.out.println("ENTER DATESTART:");
        project.setDateStart(DateUtil.parseDate(bootstrap.getSc().nextLine()));
        System.out.println("ENTER DATEFINISH:");
        project.setDateFinish(DateUtil.parseDate(bootstrap.getSc().nextLine()));
        project.setUserId(bootstrap.getCurrentUser().getId());
        if (bootstrap.getProjectService().merge(project) == null) {
            throw new EntityException();
        }
        System.out.println("[OK]");
    }
}
