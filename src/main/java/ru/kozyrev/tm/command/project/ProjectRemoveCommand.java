package ru.kozyrev.tm.command.project;

import ru.kozyrev.tm.command.AbstractCommand;

public class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove Selected project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT REMOVE]\nENTER ID:");
        String projectNum = bootstrap.getSc().nextLine();
        bootstrap.clearProject(projectNum);
    }
}
