package ru.kozyrev.tm.command.system;

import ru.kozyrev.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    public ExitCommand() {
        secure = true;
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Quit from manager.";
    }

    @Override
    public void execute() {
        bootstrap.getSc().close();
        System.exit(0);
    }
}
