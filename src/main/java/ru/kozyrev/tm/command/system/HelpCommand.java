package ru.kozyrev.tm.command.system;

import ru.kozyrev.tm.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {

    public HelpCommand() {
        secure = true;
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS LIST]");
        System.out.println("DEFAULT USERS: user1:user1, admin:admin;");
        bootstrap.printCommands();
    }
}
