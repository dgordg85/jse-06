package ru.kozyrev.tm.command.task;

import ru.kozyrev.tm.command.AbstractCommand;

public class TaskClearCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR]");
        System.out.println("Press 'ENTER' FOR ALL OR PROJECT ID]");
        String projectNum = bootstrap.getSc().nextLine();
        if (projectNum.length() == 0) {
            bootstrap.clearAllTasks();
        } else {
            bootstrap.clearTasks(projectNum);
        }
    }
}
