package ru.kozyrev.tm.command.task;

import ru.kozyrev.tm.command.AbstractCommand;

public class TaskListCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("PRESS 'ENTER' FOR ALL OR INPUT ID PROJECT");
        String projectNum = bootstrap.getSc().nextLine();
        if (projectNum.length() == 0) {
            bootstrap.printTasks();
        } else {
            bootstrap.printTasks(projectNum);
        }
    }
}
