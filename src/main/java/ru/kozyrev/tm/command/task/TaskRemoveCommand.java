package ru.kozyrev.tm.command.task;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.exception.entity.IndexException;

public class TaskRemoveCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK DELETE]\nENTER ID:");
        String taskNum = bootstrap.getSc().nextLine();
        if (bootstrap.getTaskService().remove(taskNum) == null) {
            throw new IndexException();
        }
        System.out.println("[OK]");
    }
}
