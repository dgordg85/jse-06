package ru.kozyrev.tm.entity;

import ru.kozyrev.tm.enumerated.RoleType;

import java.util.UUID;

public class User {
    private String id = UUID.randomUUID().toString();
    private String login = "";
    private String passwordHash = "";
    private RoleType roleType = RoleType.USER;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
