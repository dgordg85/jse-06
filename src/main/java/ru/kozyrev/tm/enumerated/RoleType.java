package ru.kozyrev.tm.enumerated;

public enum RoleType {
    ADMIN("Администратор"),
    USER("Обычный пользователь");

    String displayName;

    RoleType(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
